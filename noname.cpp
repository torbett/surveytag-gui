///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "noname.h"

///////////////////////////////////////////////////////////////////////////

mainWindow::mainWindow( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 2, 1, 0, 0 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_panel1 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxBORDER_THEME|wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText1 = new wxStaticText( m_panel1, wxID_ANY, wxT("Serial Port:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	bSizer4->Add( m_staticText1, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_comboBox1 = new wxComboBox( m_panel1, wxID_ANY, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	m_comboBox1->Append( wxT("COM1") );
	m_comboBox1->Append( wxT("COM2") );
	m_comboBox1->Append( wxT("etc") );
	bSizer4->Add( m_comboBox1, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_Connect = new wxButton( m_panel1, wxID_ANY, wxT("Connect"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer4->Add( m_Connect, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	cbAutoConnect = new wxCheckBox( m_panel1, wxID_ANY, wxT("Auto Connect"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer4->Add( cbAutoConnect, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	m_panel1->SetSizer( bSizer4 );
	m_panel1->Layout();
	bSizer4->Fit( m_panel1 );
	fgSizer1->Add( m_panel1, 1, wxEXPAND | wxALL, 5 );

	m_panel2 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxBORDER_THEME|wxTAB_TRAVERSAL );
	wxGridSizer* gSizer3;
	gSizer3 = new wxGridSizer( 5, 2, 0, 0 );

	m_staticText4 = new wxStaticText( m_panel2, wxID_ANY, wxT("Logger Type:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	gSizer3->Add( m_staticText4, 0, wxALL, 5 );

	m_loggerType = new wxStaticText( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_loggerType->Wrap( -1 );
	gSizer3->Add( m_loggerType, 0, wxALL, 5 );

	m_staticText7 = new wxStaticText( m_panel2, wxID_ANY, wxT("Serial Number:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText7->Wrap( -1 );
	gSizer3->Add( m_staticText7, 0, wxALL, 5 );

	m_SerNo = new wxStaticText( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_SerNo->Wrap( -1 );
	gSizer3->Add( m_SerNo, 0, wxALL, 5 );

	m_staticText9 = new wxStaticText( m_panel2, wxID_ANY, wxT("Name:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText9->Wrap( -1 );
	gSizer3->Add( m_staticText9, 0, wxALL, 5 );

	m_LoggerName = new wxStaticText( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_LoggerName->Wrap( -1 );
	gSizer3->Add( m_LoggerName, 0, wxALL, 5 );

	m_staticText11 = new wxStaticText( m_panel2, wxID_ANY, wxT("Date/Time:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText11->Wrap( -1 );
	gSizer3->Add( m_staticText11, 0, wxALL, 5 );

	m_loggerDateTime = new wxStaticText( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_loggerDateTime->Wrap( -1 );
	gSizer3->Add( m_loggerDateTime, 0, wxALL, 5 );

	m_staticText13 = new wxStaticText( m_panel2, wxID_ANY, wxT("Data in memory:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText13->Wrap( -1 );
	gSizer3->Add( m_staticText13, 0, wxALL, 5 );

	m_logRecords = new wxStaticText( m_panel2, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_logRecords->Wrap( -1 );
	gSizer3->Add( m_logRecords, 0, wxALL, 5 );


	m_panel2->SetSizer( gSizer3 );
	m_panel2->Layout();
	gSizer3->Fit( m_panel2 );
	fgSizer1->Add( m_panel2, 1, wxEXPAND | wxALL, 5 );

	m_panel4 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxBORDER_THEME|wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer3;
	fgSizer3 = new wxFlexGridSizer( 4, 4, 0, 0 );
	fgSizer3->SetFlexibleDirection( wxBOTH );
	fgSizer3->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_checkBox4 = new wxCheckBox( m_panel4, wxID_ANY, wxT("Auto Configure on connect"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer3->Add( m_checkBox4, 0, wxALL, 5 );

	m_staticText16 = new wxStaticText( m_panel4, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText16->Wrap( -1 );
	fgSizer3->Add( m_staticText16, 0, wxALL, 5 );

	m_staticText18 = new wxStaticText( m_panel4, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText18->Wrap( -1 );
	fgSizer3->Add( m_staticText18, 0, wxALL, 5 );

	m_staticText19 = new wxStaticText( m_panel4, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText19->Wrap( -1 );
	fgSizer3->Add( m_staticText19, 0, wxALL, 5 );

	m_staticText17 = new wxStaticText( m_panel4, wxID_ANY, wxT("Date/Time:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText17->Wrap( -1 );
	fgSizer3->Add( m_staticText17, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_datePicker1 = new wxDatePickerCtrl( m_panel4, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, wxDefaultSize, wxDP_DEFAULT );
	fgSizer3->Add( m_datePicker1, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_timePicker1 = new wxTimePickerCtrl( m_panel4, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, wxDefaultSize, wxTP_DEFAULT );
	fgSizer3->Add( m_timePicker1, 0, wxALL, 5 );

	m_checkBox5 = new wxCheckBox( m_panel4, wxID_ANY, wxT("Sync with PC"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer3->Add( m_checkBox5, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText161 = new wxStaticText( m_panel4, wxID_ANY, wxT("Logger Name:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText161->Wrap( -1 );
	fgSizer3->Add( m_staticText161, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl1 = new wxTextCtrl( m_panel4, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer3->Add( m_textCtrl1, 0, wxALL, 5 );

	m_checkBox6 = new wxCheckBox( m_panel4, wxID_ANY, wxT("Auto Append:"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer3->Add( m_checkBox6, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_spinCtrl3 = new wxSpinCtrl( m_panel4, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 0 );
	fgSizer3->Add( m_spinCtrl3, 0, wxALL, 5 );

	m_staticText171 = new wxStaticText( m_panel4, wxID_ANY, wxT("Log Interval:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText171->Wrap( -1 );
	fgSizer3->Add( m_staticText171, 0, wxALL, 5 );

	m_spinCtrl1 = new wxSpinCtrl( m_panel4, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 0 );
	fgSizer3->Add( m_spinCtrl1, 0, wxALL, 5 );

	m_staticText191 = new wxStaticText( m_panel4, wxID_ANY, wxT("Seconds"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText191->Wrap( -1 );
	fgSizer3->Add( m_staticText191, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_staticText20 = new wxStaticText( m_panel4, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20->Wrap( -1 );
	fgSizer3->Add( m_staticText20, 0, wxALL, 5 );

	m_staticText21 = new wxStaticText( m_panel4, wxID_ANY, wxT("Start Condition:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText21->Wrap( -1 );
	fgSizer3->Add( m_staticText21, 0, wxALL, 5 );

	m_radioBtn1 = new wxRadioButton( m_panel4, wxID_ANY, wxT("Immediate"), wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	fgSizer3->Add( m_radioBtn1, 0, wxALL, 5 );

	m_radioBtn2 = new wxRadioButton( m_panel4, wxID_ANY, wxT("Button Press"), wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	m_radioBtn2->SetValue( true );
	fgSizer3->Add( m_radioBtn2, 0, wxALL, 5 );

	m_staticText22 = new wxStaticText( m_panel4, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText22->Wrap( -1 );
	fgSizer3->Add( m_staticText22, 0, wxALL, 5 );

	m_button3 = new wxButton( m_panel4, wxID_ANY, wxT("Configure"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer3->Add( m_button3, 0, wxALL, 5 );


	m_panel4->SetSizer( fgSizer3 );
	m_panel4->Layout();
	fgSizer3->Fit( m_panel4 );
	fgSizer1->Add( m_panel4, 1, wxEXPAND | wxALL, 5 );

	m_panel3 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxBORDER_THEME|wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer( 0, 4, 0, 0 );
	fgSizer2->SetFlexibleDirection( wxBOTH );
	fgSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_button4 = new wxButton( m_panel3, wxID_ANY, wxT("Download"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_button4, 0, wxALL, 5 );

	m_checkBox2 = new wxCheckBox( m_panel3, wxID_ANY, wxT("Auto Download"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_checkBox2, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_checkBox3 = new wxCheckBox( m_panel3, wxID_ANY, wxT("Erase after Download"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_checkBox3, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_checkBox7 = new wxCheckBox( m_panel3, wxID_ANY, wxT("Put Logger in Standby"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_checkBox7, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	m_panel3->SetSizer( fgSizer2 );
	m_panel3->Layout();
	fgSizer2->Fit( m_panel3 );
	fgSizer1->Add( m_panel3, 1, wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxVERTICAL );

	m_gauge1 = new wxGauge( this, wxID_ANY, 100, wxDefaultPosition, wxSize( 800,-1 ), wxGA_SMOOTH );
	m_gauge1->SetValue( 50 );
	bSizer2->Add( m_gauge1, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );


	fgSizer1->Add( bSizer2, 1, wxEXPAND, 5 );


	this->SetSizer( fgSizer1 );
	this->Layout();
	fgSizer1->Fit( this );

	this->Centre( wxBOTH );
}

mainWindow::~mainWindow()
{
}
