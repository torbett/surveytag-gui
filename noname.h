///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/combobox.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/checkbox.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/datectrl.h>
#include <wx/dateevt.h>
#include <wx/timectrl.h>
#include <wx/textctrl.h>
#include <wx/spinctrl.h>
#include <wx/radiobut.h>
#include <wx/gauge.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class mainWindow
///////////////////////////////////////////////////////////////////////////////
class mainWindow : public wxFrame
{
	private:

	protected:
		wxPanel* m_panel1;
		wxStaticText* m_staticText1;
		wxComboBox* m_comboBox1;
		wxButton* m_Connect;
		wxCheckBox* cbAutoConnect;
		wxPanel* m_panel2;
		wxStaticText* m_staticText4;
		wxStaticText* m_loggerType;
		wxStaticText* m_staticText7;
		wxStaticText* m_SerNo;
		wxStaticText* m_staticText9;
		wxStaticText* m_LoggerName;
		wxStaticText* m_staticText11;
		wxStaticText* m_loggerDateTime;
		wxStaticText* m_staticText13;
		wxStaticText* m_logRecords;
		wxPanel* m_panel4;
		wxCheckBox* m_checkBox4;
		wxStaticText* m_staticText16;
		wxStaticText* m_staticText18;
		wxStaticText* m_staticText19;
		wxStaticText* m_staticText17;
		wxDatePickerCtrl* m_datePicker1;
		wxTimePickerCtrl* m_timePicker1;
		wxCheckBox* m_checkBox5;
		wxStaticText* m_staticText161;
		wxTextCtrl* m_textCtrl1;
		wxCheckBox* m_checkBox6;
		wxSpinCtrl* m_spinCtrl3;
		wxStaticText* m_staticText171;
		wxSpinCtrl* m_spinCtrl1;
		wxStaticText* m_staticText191;
		wxStaticText* m_staticText20;
		wxStaticText* m_staticText21;
		wxRadioButton* m_radioBtn1;
		wxRadioButton* m_radioBtn2;
		wxStaticText* m_staticText22;
		wxButton* m_button3;
		wxPanel* m_panel3;
		wxButton* m_button4;
		wxCheckBox* m_checkBox2;
		wxCheckBox* m_checkBox3;
		wxCheckBox* m_checkBox7;
		wxGauge* m_gauge1;

	public:

		mainWindow( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Logger GUI"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~mainWindow();

};

