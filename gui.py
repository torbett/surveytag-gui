# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.adv

###########################################################################
## Class mainWindow
###########################################################################

class mainWindow ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Logger GUI", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		fgSizer1 = wx.FlexGridSizer( 6, 1, 0, 0 )
		fgSizer1.SetFlexibleDirection( wx.BOTH )
		fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.BORDER_THEME|wx.TAB_TRAVERSAL )
		bSizer4 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText1 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Serial Port:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1.Wrap( -1 )

		bSizer4.Add( self.m_staticText1, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		m_comboBox1Choices = [ u"COM1", u"COM2", u"etc" ]
		self.m_comboBox1 = wx.ComboBox( self.m_panel1, wx.ID_ANY, u"Select", wx.DefaultPosition, wx.DefaultSize, m_comboBox1Choices, 0 )
		bSizer4.Add( self.m_comboBox1, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_Connect = wx.Button( self.m_panel1, wx.ID_ANY, u"Connect", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer4.Add( self.m_Connect, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.cbAutoConnect = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"Auto Connect", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer4.Add( self.cbAutoConnect, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		self.m_panel1.SetSizer( bSizer4 )
		self.m_panel1.Layout()
		bSizer4.Fit( self.m_panel1 )
		fgSizer1.Add( self.m_panel1, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_panel2 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.BORDER_THEME|wx.TAB_TRAVERSAL )
		gSizer3 = wx.GridSizer( 5, 2, 0, 0 )

		self.m_staticText4 = wx.StaticText( self.m_panel2, wx.ID_ANY, u"Logger Type:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText4.Wrap( -1 )

		gSizer3.Add( self.m_staticText4, 0, wx.ALL, 5 )

		self.m_loggerType = wx.StaticText( self.m_panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_loggerType.Wrap( -1 )

		gSizer3.Add( self.m_loggerType, 0, wx.ALL, 5 )

		self.m_staticText7 = wx.StaticText( self.m_panel2, wx.ID_ANY, u"Serial Number:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )

		gSizer3.Add( self.m_staticText7, 0, wx.ALL, 5 )

		self.m_SerNo = wx.StaticText( self.m_panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_SerNo.Wrap( -1 )

		gSizer3.Add( self.m_SerNo, 0, wx.ALL, 5 )

		self.m_staticText9 = wx.StaticText( self.m_panel2, wx.ID_ANY, u"Name:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText9.Wrap( -1 )

		gSizer3.Add( self.m_staticText9, 0, wx.ALL, 5 )

		self.m_LoggerName = wx.StaticText( self.m_panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_LoggerName.Wrap( -1 )

		gSizer3.Add( self.m_LoggerName, 0, wx.ALL, 5 )

		self.m_staticText11 = wx.StaticText( self.m_panel2, wx.ID_ANY, u"Date/Time:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText11.Wrap( -1 )

		gSizer3.Add( self.m_staticText11, 0, wx.ALL, 5 )

		self.m_loggerDateTime = wx.StaticText( self.m_panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_loggerDateTime.Wrap( -1 )

		gSizer3.Add( self.m_loggerDateTime, 0, wx.ALL, 5 )

		self.m_staticText13 = wx.StaticText( self.m_panel2, wx.ID_ANY, u"Data in memory:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText13.Wrap( -1 )

		gSizer3.Add( self.m_staticText13, 0, wx.ALL, 5 )

		self.m_logRecords = wx.StaticText( self.m_panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_logRecords.Wrap( -1 )

		gSizer3.Add( self.m_logRecords, 0, wx.ALL, 5 )


		self.m_panel2.SetSizer( gSizer3 )
		self.m_panel2.Layout()
		gSizer3.Fit( self.m_panel2 )
		fgSizer1.Add( self.m_panel2, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_panel4 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.BORDER_THEME|wx.TAB_TRAVERSAL )
		fgSizer3 = wx.FlexGridSizer( 6, 4, 0, 0 )
		fgSizer3.SetFlexibleDirection( wx.BOTH )
		fgSizer3.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_checkBox4 = wx.CheckBox( self.m_panel4, wx.ID_ANY, u"Auto Configure on connect", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer3.Add( self.m_checkBox4, 0, wx.ALL, 5 )

		self.m_staticText16 = wx.StaticText( self.m_panel4, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText16.Wrap( -1 )

		fgSizer3.Add( self.m_staticText16, 0, wx.ALL, 5 )

		self.m_staticText18 = wx.StaticText( self.m_panel4, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText18.Wrap( -1 )

		fgSizer3.Add( self.m_staticText18, 0, wx.ALL, 5 )

		self.m_staticText19 = wx.StaticText( self.m_panel4, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText19.Wrap( -1 )

		fgSizer3.Add( self.m_staticText19, 0, wx.ALL, 5 )

		self.m_staticText17 = wx.StaticText( self.m_panel4, wx.ID_ANY, u"Date/Time:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText17.Wrap( -1 )

		fgSizer3.Add( self.m_staticText17, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_datePicker1 = wx.adv.DatePickerCtrl( self.m_panel4, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.adv.DP_DEFAULT )
		fgSizer3.Add( self.m_datePicker1, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_timePicker1 = wx.adv.TimePickerCtrl( self.m_panel4, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.adv.TP_DEFAULT )
		fgSizer3.Add( self.m_timePicker1, 0, wx.ALL, 5 )

		self.m_checkBox5 = wx.CheckBox( self.m_panel4, wx.ID_ANY, u"Sync with PC", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer3.Add( self.m_checkBox5, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_staticText161 = wx.StaticText( self.m_panel4, wx.ID_ANY, u"Logger Name:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText161.Wrap( -1 )

		fgSizer3.Add( self.m_staticText161, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_textCtrl1 = wx.TextCtrl( self.m_panel4, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer3.Add( self.m_textCtrl1, 0, wx.ALL, 5 )

		self.m_checkBox6 = wx.CheckBox( self.m_panel4, wx.ID_ANY, u"Auto Append:", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer3.Add( self.m_checkBox6, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_spinCtrl3 = wx.SpinCtrl( self.m_panel4, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 10, 0 )
		fgSizer3.Add( self.m_spinCtrl3, 0, wx.ALL, 5 )

		self.m_staticText171 = wx.StaticText( self.m_panel4, wx.ID_ANY, u"Log Interval:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText171.Wrap( -1 )

		fgSizer3.Add( self.m_staticText171, 0, wx.ALL, 5 )

		self.m_spinCtrl1 = wx.SpinCtrl( self.m_panel4, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 10, 0 )
		fgSizer3.Add( self.m_spinCtrl1, 0, wx.ALL, 5 )

		self.m_staticText191 = wx.StaticText( self.m_panel4, wx.ID_ANY, u"Seconds", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText191.Wrap( -1 )

		fgSizer3.Add( self.m_staticText191, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_staticText20 = wx.StaticText( self.m_panel4, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText20.Wrap( -1 )

		fgSizer3.Add( self.m_staticText20, 0, wx.ALL, 5 )

		self.m_staticText21 = wx.StaticText( self.m_panel4, wx.ID_ANY, u"Start Condition:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText21.Wrap( -1 )

		fgSizer3.Add( self.m_staticText21, 0, wx.ALL, 5 )

		self.m_radioBtn1 = wx.RadioButton( self.m_panel4, wx.ID_ANY, u"Immediate", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer3.Add( self.m_radioBtn1, 0, wx.ALL, 5 )

		self.m_radioBtn2 = wx.RadioButton( self.m_panel4, wx.ID_ANY, u"Button Press", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_radioBtn2.SetValue( True )
		fgSizer3.Add( self.m_radioBtn2, 0, wx.ALL, 5 )

		self.m_staticText22 = wx.StaticText( self.m_panel4, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText22.Wrap( -1 )

		fgSizer3.Add( self.m_staticText22, 0, wx.ALL, 5 )

		self.m_button3 = wx.Button( self.m_panel4, wx.ID_ANY, u"Configure", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer3.Add( self.m_button3, 0, wx.ALL, 5 )


		self.m_panel4.SetSizer( fgSizer3 )
		self.m_panel4.Layout()
		fgSizer3.Fit( self.m_panel4 )
		fgSizer1.Add( self.m_panel4, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_panel3 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.BORDER_THEME|wx.TAB_TRAVERSAL )
		fgSizer2 = wx.FlexGridSizer( 0, 4, 0, 0 )
		fgSizer2.SetFlexibleDirection( wx.BOTH )
		fgSizer2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_button4 = wx.Button( self.m_panel3, wx.ID_ANY, u"Download", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer2.Add( self.m_button4, 0, wx.ALL, 5 )

		self.m_checkBox2 = wx.CheckBox( self.m_panel3, wx.ID_ANY, u"Auto Download", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer2.Add( self.m_checkBox2, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_checkBox3 = wx.CheckBox( self.m_panel3, wx.ID_ANY, u"Erase after Download", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer2.Add( self.m_checkBox3, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_checkBox7 = wx.CheckBox( self.m_panel3, wx.ID_ANY, u"Put Logger in Standby", wx.DefaultPosition, wx.DefaultSize, 0 )
		fgSizer2.Add( self.m_checkBox7, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		self.m_panel3.SetSizer( fgSizer2 )
		self.m_panel3.Layout()
		fgSizer2.Fit( self.m_panel3 )
		fgSizer1.Add( self.m_panel3, 1, wx.EXPAND |wx.ALL, 5 )

		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		self.m_gauge1 = wx.Gauge( self, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size( 800,-1 ), wx.GA_SMOOTH )
		self.m_gauge1.SetValue( 50 )
		bSizer2.Add( self.m_gauge1, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		fgSizer1.Add( bSizer2, 1, wx.EXPAND, 5 )


		self.SetSizer( fgSizer1 )
		self.Layout()
		fgSizer1.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_Connect.Bind( wx.EVT_BUTTON, self.connectButtonClick )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def connectButtonClick( self, event ):
		event.Skip()



myApp = wx.App()
frame = mainWindow(parent=None)
frame.Show()
myApp.MainLoop()

